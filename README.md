All the public git projects Evan's working on at any given time, configured using the `meta` tool: https://github.com/mateodelnorte/meta

To get started:

    yarn global add meta
    meta git clone git@gitlab.com:erosson/metarepo.git workspace
    cd workspace
    # or, if already cloned: `meta git update`

To clone a new project into meta:

    meta project add src/PROJECT PROJECT-URL
    meta git pull origin master
    meta exec 'git push -u origin master'

To add a project to meta:

    env PROJECT=src/NAME sh -c 'meta project add $PROJECT $(cd $PROJECT && git remote get-url origin)'
