I store my Atom editor configuration in Git, and separate it by project-type.
* It's useful to know what packages I had installed when I reformat my machine. 
* It's useful to have a separate set of Atom packages installed for Elm projects than for Go projects, for example. Installing everything in one profile is problematic if an extension starts misbehaving and impacting other projects' setup.
